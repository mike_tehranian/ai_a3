"""Testing pbnt. Run this before anything else to get pbnt to work!"""
import sys

if('pbnt/combined' not in sys.path):
    sys.path.append('pbnt/combined')
from exampleinference import inferenceExample

inferenceExample()
# Should output:
# ('The marginal probability of sprinkler=false:', 0.80102921)
#('The marginal probability of wetgrass=false | cloudy=False, rain=True:', 0.055)

'''
WRITE YOUR CODE BELOW. DO NOT CHANGE ANY FUNCTION HEADERS FROM THE NOTEBOOK.
'''


from Node import BayesNode
from Graph import BayesNet
from numpy import zeros, float32
import Distribution
from Distribution import DiscreteDistribution, ConditionalDiscreteDistribution
from Inference import JunctionTreeEngine


def make_power_plant_net():
    """Create a Bayes Net representation of the above power plant problem.
    Use the following as the name attribute: "alarm","faulty alarm", "gauge","faulty gauge", "temperature". (for the tests to work.)
    """
    nodes = []

    tNode = BayesNode(0, 2, name="temperature")
    fgNode = BayesNode(1, 2, name="faulty gauge")
    gNode = BayesNode(2, 2, name="gauge")
    faNode = BayesNode(3, 2, name="faulty alarm")
    aNode = BayesNode(4, 2, name="alarm")

    # Temperature node
    tNode.add_child(gNode)
    tNode.add_child(fgNode)

    # Gauge node
    gNode.add_child(aNode)
    gNode.add_parent(tNode)
    gNode.add_parent(fgNode)

    # Faulty Gauge node
    fgNode.add_child(gNode)
    fgNode.add_parent(tNode)

    # Faulty Alarm node
    faNode.add_child(aNode)

    # Alarm node
    aNode.add_parent(gNode)
    aNode.add_parent(faNode)

    nodes = [tNode, fgNode, gNode, faNode, aNode]

    return BayesNet(nodes)

def set_probability(bayes_net):
    """Set probability distribution for each node in the power plant system."""
    T_node = bayes_net.get_node_by_name("temperature")
    F_G_node = bayes_net.get_node_by_name("faulty gauge")
    G_node = bayes_net.get_node_by_name("gauge")
    F_A_node = bayes_net.get_node_by_name("faulty alarm")
    A_node = bayes_net.get_node_by_name("alarm")
    nodes = [A_node, F_A_node, G_node, F_G_node, T_node]

    # Temperature node distribution
    T_distribution = DiscreteDistribution(T_node)
    index = T_distribution.generate_index([],[])
    T_distribution[index] = [0.8, 0.2]
    T_node.set_dist(T_distribution)

    # Faulty Gauge node distribution
    F_G_dist = zeros([T_node.size(), F_G_node.size()], dtype=float32)
    F_G_dist[0,:] = [0.95, 0.05]
    F_G_dist[1,:] = [0.2, 0.8]
    F_G_distribution = ConditionalDiscreteDistribution(nodes=[T_node, F_G_node], table=F_G_dist)
    F_G_node.set_dist(F_G_distribution)

    # Gauge node distribution
    G_dist = zeros([T_node.size(), F_G_node.size(), G_node.size()], dtype=float32)
    G_dist[0,0,:] = [0.95, 0.05]
    G_dist[0,1,:] = [0.2, 0.8]
    G_dist[1,0,:] = [0.05, 0.95]
    G_dist[1,1,:] = [0.8, 0.2]
    G_distribution = ConditionalDiscreteDistribution(nodes=[T_node, F_G_node, G_node], table=G_dist)
    G_node.set_dist(G_distribution)

    # Faulty Alarm node distribution
    F_A_distribution = DiscreteDistribution(F_A_node)
    index = F_A_distribution.generate_index([],[])
    F_A_distribution[index] = [0.85, 0.15]
    F_A_node.set_dist(F_A_distribution)

    # Alarm node distribution
    A_dist = zeros([G_node.size(), F_A_node.size(), A_node.size()], dtype=float32)
    A_dist[0,0,:] = [0.9, 0.1]
    A_dist[0,1,:] = [0.55, 0.45]
    A_dist[1,0,:] = [0.1, 0.9]
    A_dist[1,1,:] = [0.45, 0.55]
    A_distribution = ConditionalDiscreteDistribution(nodes=[G_node, F_A_node, A_node], table=A_dist)
    A_node.set_dist(A_distribution)

    return bayes_net

def get_alarm_prob(bayes_net, alarm_rings):
    """Calculate the marginal
    probability of the alarm
    ringing (T/F) in the
    power plant system."""

    aNode = bayes_net.get_node_by_name("alarm")
    engine = JunctionTreeEngine(bayes_net)
    # Set the variable that we are examining
    Q = engine.marginal(aNode)[0]
    # Set the result we are looking for, depending on the function parameter
    index = Q.generate_index([alarm_rings],range(Q.nDims))
    prob = Q[index]

    return prob


def get_gauge_prob(bayes_net, gauge_hot):
    """Calculate the marginal
    probability of the gauge
    showing hot (T/F) in the
    power plant system."""

    gNode = bayes_net.get_node_by_name("gauge")
    engine = JunctionTreeEngine(bayes_net)
    # Set the variable that we are examining
    Q = engine.marginal(gNode)[0]
    # Set the result we are looking for, depending on the function parameter
    index = Q.generate_index([gauge_hot],range(Q.nDims))
    prob = Q[index]

    return prob


def get_temperature_prob(bayes_net,temp_hot):
    """Calculate the conditional probability
    of the temperature being hot (T/F) in the
    power plant system, given that the
    alarm sounds and neither the gauge
    nor alarm is faulty."""

    tNode = bayes_net.get_node_by_name("temperature")
    fgNode = bayes_net.get_node_by_name("faulty gauge")
    faNode = bayes_net.get_node_by_name("faulty alarm")
    aNode = bayes_net.get_node_by_name("alarm")

    engine = JunctionTreeEngine(bayes_net)
    # Set the evidence for the conditional variables
    engine.evidence[fgNode] = False
    engine.evidence[faNode] = False
    engine.evidence[aNode] = True
    Q = engine.marginal(tNode)[0]
    index = Q.generate_index([temp_hot],range(Q.nDims))
    prob = Q[index]

    return prob


def get_game_network():
    """Create a Bayes Net representation of the game problem.
    Name the nodes as "A","B","C","AvB","BvC" and "CvA".  """
    nodes = []

    num_skill_levels = 4

    a_node = BayesNode(0, num_skill_levels, name="A")
    b_node = BayesNode(1, num_skill_levels, name="B")
    c_node = BayesNode(2, num_skill_levels, name="C")
    # Three is win, loss or tie
    AvB_node = BayesNode(3, 3, name="AvB")
    BvC_node = BayesNode(4, 3, name="BvC")
    CvA_node = BayesNode(5, 3, name="CvA")

    # Team A node
    a_node.add_child(AvB_node)
    a_node.add_child(CvA_node)

    # Team B node
    b_node.add_child(AvB_node)
    b_node.add_child(BvC_node)

    # Team C node
    c_node.add_child(BvC_node)
    c_node.add_child(CvA_node)

    # Team AB node
    AvB_node.add_parent(a_node)
    AvB_node.add_parent(b_node)

    # Team BC node
    BvC_node.add_parent(c_node)
    BvC_node.add_parent(b_node)

    # Team CA node
    CvA_node.add_parent(a_node)
    CvA_node.add_parent(c_node)

    player_skill_prob = [0.15, 0.45, 0.3, 0.1]

    # Team A distribution
    a_dist = DiscreteDistribution(a_node)
    index = a_dist.generate_index([], [])
    a_dist[index] = list(player_skill_prob)
    a_node.set_dist(a_dist)

    # Team B distribution
    b_dist = DiscreteDistribution(b_node)
    index = b_dist.generate_index([], [])
    b_dist[index] = list(player_skill_prob)
    b_node.set_dist(b_dist)

    # Team C distribution
    c_dist = DiscreteDistribution(c_node)
    index = c_dist.generate_index([],[])
    c_dist[index] = list(player_skill_prob)
    c_node.set_dist(c_dist)

    skills_difference_probabilities = {
        -3: [0.9, 0.05, 0.05], -2: [0.75, 0.15, 0.1], -1: [0.6, 0.2, 0.2],
        0: [0.1, 0.1, 0.8], 1: [0.2, 0.6, 0.2], 2: [0.15, 0.75, 0.1], 3: [0.05, 0.9, 0.05]
    }

    AvB_dist = zeros([a_node.size(), b_node.size(), AvB_node.size()], dtype=float32)
    for a_skill in range(num_skill_levels):
        for b_skill in range(num_skill_levels):
            t2_minus_t1_diff = b_skill - a_skill
            AvB_dist[a_skill, b_skill,:] = skills_difference_probabilities[t2_minus_t1_diff]
    AvB_node.set_dist(ConditionalDiscreteDistribution(nodes=[a_node, b_node, AvB_node], table=AvB_dist))

    BvC_dist = zeros([b_node.size(), c_node.size(), BvC_node.size()], dtype=float32)
    for b_skill in range(num_skill_levels):
        for c_skill in range(num_skill_levels):
            t2_minus_t1_diff = c_skill - b_skill
            BvC_dist[b_skill, c_skill,:] = skills_difference_probabilities[t2_minus_t1_diff]
    BvC_node.set_dist(ConditionalDiscreteDistribution(nodes=[b_node, c_node, BvC_node], table=BvC_dist))

    CvA_dist = zeros([c_node.size(), a_node.size(), CvA_node.size()], dtype=float32)
    for c_skill in range(num_skill_levels):
        for a_skill in range(num_skill_levels):
            t2_minus_t1_diff = a_skill - c_skill
            CvA_dist[c_skill, a_skill,:] = skills_difference_probabilities[t2_minus_t1_diff]
    CvA_node.set_dist(ConditionalDiscreteDistribution(nodes=[c_node, a_node, CvA_node], table=CvA_dist))

    nodes = [a_node, b_node, c_node, AvB_node, BvC_node, CvA_node]

    return BayesNet(nodes)

def calculate_posterior(bayes_net):
    """Calculate the posterior distribution of the BvC match given that A won against B and tied C.
    Return a list of probabilities corresponding to win, loss and tie likelihood."""
    from Inference import EnumerationEngine

    posterior = [0,0,0]

    ab_node = bayes_net.get_node_by_name("AvB")
    bc_node = bayes_net.get_node_by_name("BvC")
    ca_node = bayes_net.get_node_by_name("CvA")

    engine = EnumerationEngine(bayes_net)
    # A won against B
    engine.evidence[ab_node] = 0
    # A and C tied
    engine.evidence[ca_node] = 2

    # Solve for BvC
    Q = engine.marginal(bc_node)[0]
    idx = Q.generate_index([], [])
    posterior = list(Q[idx])

    return posterior # list


def Gibbs_sampler(bayes_net, initial_state):
    """Complete a single iteration of the Gibbs sampling algorithm
    given a Bayesian network and an initial state value.

    initial_state is a list of length 6 where:
    index 0-2: represent skills of teams A,B,C (values lie in [0,3] inclusive)
    index 3-5: represent results of matches AvB, BvC, CvA (values lie in [0,2] inclusive)

    Returns the new state sampled from the probability distribution as a tuple of length 6.
    Return the sample as a tuple.
    """
    import numpy as np

    # Any team node suffices
    team_node = bayes_net.get_node_by_name("A")
    team_table = team_node.dist.table
    # Any match node suffices
    match_node = bayes_net.get_node_by_name("AvB")
    match_table = match_node.dist.table

    num_teams = 3
    num_matches = 3
    num_states = num_teams + num_matches
    num_skill_levels = team_node.size()
    num_match_outcomes = match_node.size()

    delta_index = np.random.randint(num_states)

    # If an initial value is not given
    if not initial_state:
        # Default to a state chosen uniformly at random from the possible states
        initial_state = np.random.randint(4, size=num_teams) + \
            np.random.randint(3, size=num_matches)

    # Convert the incoming state (if a tuple) to a list
    initial_state = list(initial_state)

    if delta_index < num_teams:
        # If RNG selected a team node
        p_new_skill_levels = np.zeros(num_skill_levels)
        for a_skill_level in range(num_skill_levels):
            p_a_skill_level = team_table[a_skill_level]

            b_skill_level = initial_state[(delta_index + 1) % num_teams]
            a_b_match_state = initial_state[delta_index + num_teams]
            p_b_skill_level = team_table[(delta_index + 1) % num_teams]

            c_skill_level = initial_state[(delta_index - 1) % num_teams]
            c_a_match_state = initial_state[((delta_index - 1) % num_teams) + num_teams]
            p_c_skill_level = team_table[(delta_index - 1) % num_teams]

            # Calculate the probability over the Markov blanket
            p_new_skill_levels[a_skill_level] = p_a_skill_level * \
                match_table[a_skill_level, b_skill_level, a_b_match_state] * \
                match_table[c_skill_level, a_skill_level, c_a_match_state] * \
                p_b_skill_level * \
                p_c_skill_level
        prob_weight = np.sum(p_new_skill_levels)
        # Broadcast the weights to normalize the probabilities
        new_team_prob = p_new_skill_levels / prob_weight
        initial_state[delta_index] = np.random.choice(num_skill_levels, p=new_team_prob)
    else:
        # If RNG selected a match node
        a_index = delta_index - num_teams
        b_index = (delta_index + 1 - num_teams) % num_teams
        a_skill_level = initial_state[a_index]
        b_skill_level = initial_state[b_index]
        new_match_prob = match_table[a_skill_level, b_skill_level,:]
        initial_state[delta_index] = np.random.choice(num_match_outcomes, p=new_match_prob)

    sample = tuple(initial_state)

    return sample


def MH_sampler(bayes_net, initial_state):
    """Complete a single iteration of the MH sampling algorithm given a Bayesian network and an initial state value.
    initial_state is a list of length 6 where:
    index 0-2: represent skills of teams A,B,C (values lie in [0,3] inclusive)
    index 3-5: represent results of matches AvB, BvC, CvA (values lie in [0,2] inclusive)

    Returns the new state sampled from the probability distribution as a tuple of length 6.
    """
    import numpy as np
    import random

    # Any team node suffices
    team_node = bayes_net.get_node_by_name("A")
    team_table = team_node.dist.table
    # Any match node suffices
    match_node = bayes_net.get_node_by_name("AvB")
    match_table = match_node.dist.table

    num_teams = 3
    num_matches = 3
    num_states = num_teams + num_matches
    num_skill_levels = 4
    num_match_outcomes = 3

    # If an initial value is not given
    if not initial_state:
        # Default to a state chosen uniformly at random from the possible states
        initial_state = np.random.randint(4, size=num_teams) + \
            np.random.randint(3, size=num_matches)

    # Convert the incoming state (if a tuple) to a list
    initial_state = list(initial_state)

    # Assign random states to each node
    new_state = [None] * num_states
    for team in range(num_teams):
        new_state[team] = np.random.randint(num_skill_levels)
    for match in range(num_teams, num_states):
        new_state[match] = np.random.randint(num_match_outcomes)

    # Calculate probabilities
    p_initial_state = 1.0
    p_new_state = 1.0
    for team in range(num_teams):
        initial_team_skill = initial_state[team]
        new_team_skill = new_state[team]
        p_initial_state *= team_table[initial_team_skill]
        p_new_state *= team_table[new_team_skill]
    for match in range(num_teams, num_states):
        match_player_a = match - num_teams
        match_player_b = (match - num_teams + 1) % num_teams
        a_skill_level = initial_state[match_player_a]
        b_skill_level = initial_state[match_player_b]
        b_a_skill_diff = initial_state[match]
        p_initial_state *= match_table[a_skill_level, b_skill_level, b_a_skill_diff]
        a_skill_level_new = new_state[match_player_a]
        b_skill_level_new = new_state[match_player_b]
        b_a_skill_diff_new = new_state[match]
        p_new_state *= match_table[a_skill_level_new, b_skill_level_new, b_a_skill_diff_new]

    rho = p_new_state / p_initial_state
    alpha = min(1, rho)

    u = random.uniform(0, 1)
    if u < alpha:
        # Accept the proposal
        sample = tuple(new_state)
    else:
        # Reject the proposal
        sample = tuple(initial_state)

    return sample


def compare_sampling(bayes_net,initial_state, delta):
    """Compare Gibbs and Metropolis-Hastings sampling by calculating how long it takes for each method to converge."""
    Gibbs_count = 0
    MH_count = 0
    MH_rejection_count = 0
    Gibbs_convergence = [0,0,0] # posterior distribution of the BvC match as produced by Gibbs
    MH_convergence = [0,0,0] # posterior distribution of the BvC match as produced by MH

    if delta is None:
        delta = 0.001

    # If an initial value is not given
    if not initial_state:
        # Default to a state chosen uniformly at random from the possible states
        initial_state = np.random.randint(4, size=num_teams) + \
            np.random.randint(3, size=num_matches)

    original_initial_state = list(initial_state)

    # Convert tuple to list
    state_with_evidence = list(initial_state)
    # AvB - A wins
    state_with_evidence[3] = 0
    # CvA - Draw
    state_with_evidence[5] = 2

    state_with_evidence_gibbs = list(state_with_evidence)
    state_with_evidence_mh = list(state_with_evidence)

    Gibbs_convergence, Gibbs_count = convergence(bayes_net, state_with_evidence_gibbs, delta, "Gibbs")
    MH_convergence, MH_count, MH_rejection_count = convergence(bayes_net, state_with_evidence_mh, delta, "MH")

    return Gibbs_convergence, MH_convergence, Gibbs_count, MH_count, MH_rejection_count

def convergence(bayes_net, initial_state, delta, mcmc_algo):
    algo_fn = Gibbs_sampler if mcmc_algo == 'Gibbs' else MH_sampler

    num_burn_iterations = 1000
    num_iterations = 100000
    total_count = 0
    num_rejections = 0
    num_invalid = 0
    match_counts = [0, 0, 0]

    prev_state = list(initial_state)
    new_state = list(initial_state)
    previous_posterior = None
    current_posterior = [0,0,0]

    # Burn-in iterations
    for _ in range(num_burn_iterations):
        new_state = algo_fn(bayes_net, new_state)
        # Ignore samples where our given evidence does not hold
        if not (new_state[3] == 0 and new_state[5] == 2):
            # Do not increase the total_count used for probability calculations
            num_invalid += 1
            new_state = list(prev_state)
            continue
        if new_state == prev_state:
            num_rejections += 1
            total_count += 1
            prev_state = list(new_state)
            continue
        else:
            total_count += 1
            prev_state = list(new_state)

    while (num_invalid + total_count + num_rejections) < num_iterations:
        new_state = algo_fn(bayes_net, new_state)

        if not (new_state[3] == 0 and new_state[5] == 2):
            # Do not increase the total_count used for probability calculations
            num_invalid += 1
            continue
        if new_state == prev_state:
            num_rejections += 1
            # Check and Update counts for BvC
            bvc_game_result = new_state[4]
            match_counts[bvc_game_result] += 1
            total_count += 1
            continue
        else:
            # Check and Update counts for BvC
            bvc_game_result = new_state[4]
            match_counts[bvc_game_result] += 1
            total_count += 1
        prev_state = list(new_state)

        # The sampler has converged when, for 10 successive iterations,
        # The difference in expected outcome for the third match differs from the
        # previous estimated outcome by less than .001 (0.1%).
        if total_count % 10 == 0:
            num_trials = sum(match_counts)
            current_posterior = [
                    float(outcome) / num_trials
                    for outcome in match_counts
            ]

            if previous_posterior is not None:
                epsilons = [
                    abs(current_posterior[i] - previous_posterior[i]) < delta
                    for i in range(len(current_posterior))
                ]
                if all(epsilons):
                    if mcmc_algo == 'Gibbs':
                        return current_posterior, (total_count + num_burn_iterations)
                    else:
                        return current_posterior, (total_count + num_burn_iterations), num_rejections

            previous_posterior = list(current_posterior)

    # No convergence
    if mcmc_algo == 'Gibbs':
        return current_posterior, (total_count + num_burn_iterations)
    else:
        return current_posterior, (total_count + num_burn_iterations), num_rejections


def sampling_question():
    """Question about sampling performance."""
    choice = 1
    options = ['Gibbs','Metropolis-Hastings']
    factor = 1
    return options[choice], factor
